import gql from 'graphql-tag';

export const GET_ALL_FORESTS = gql`
query GET_ALL_FORESTS {
    allForests {
        nodes {
          forestName
          id
          nodeId
          forestGeoLat
          forestGeoLong
          forestGeoRadius
          forestCarbonoffset
          forestOxygen    
          forestCountry
          forestDesc
          forestCity
          forestAddress
          forestGeoShape
          forestUrl
          forestSponsorsByForestId {
            nodes {
              sponsorBySponsorId {
                sponsorName
                sponsorLogo
              }
            }
          }
          forestTreesByForestId {
            nodes {
              forestTreeName
              id
              masterPlantspecyByMasterPlantSpeciesId {
                speciesName
              }
            }
            totalCount
          }
        }
      }
  }
`;

export const GET_FOREST_BY_NODEID = gql`
query GET_FOREST_BY_NODEID($nodeId:ID!){
    forest(
        nodeId: $nodeId
      ) {
        forestTreesByForestId {
          nodes {
            forestTreeName
            forestTreeHeight
            forestTreeGeoLong
            id
            nodeId
            forestTreeGeoLat
          }
        }
        forestSponsorsByForestId {
          nodes {
            sponsorBySponsorId {
              sponsorName
            }
          }
        }
      }
  }
`;

export const GET_TREES_BY_NODEID = gql`
query GET_TREES_BY_NODEID($nodeId:ID!){
    forestTree(
        nodeId: $nodeId
    ) {
      forestTreeName
      forestTreeOxygen
      forestTreePetname
      forestTreeCarbonoffset
      forestTreeDia
      forestTreeGeoLat
      forestTreeGeoLong
      forestTreeHeight
      forestTreeAge
      masterPlantspecyByMasterPlantSpeciesId {
        speciesName
        speciesOxygenLevel1
        speciesOxygenLevel2
        speciesOxygenLevel3
        speciesOxygenLevel4
        speciesOxygenLevel5
      }
      forestTreeSponsorsByForestTreeId {
        nodes {
          sponsorBySponsorId {
            sponsorName
            id
          }
        }
      }
      plantedBy
      treeAssertsByTreeId {
        nodes {
          type
          url
          isActive
        }
      }
    }
  }
`;

export const GET_SEARCH_LIST = gql`
query GET_SEARCH_LIST($search: String) {
    forest: allForests(filter: {forestName: {includes: $search}}) {
      nodes {
        forestName
        nodeId
        forestGeoLat
        forestGeoLong
      }
    }
    trees: allForestTrees(filter: {forestTreeName: {includes: $search}}) {
      nodes {
        forestTreeName
        nodeId
        forestTreeGeoLong
        forestTreeGeoLat
        forestByForestId {
          forestName
          nodeId
        }
      }
    }
  }
`

// export const GET_ALL_FORESTS_AND_TREES_PAGINATION = gql`
// query GET_ALL_FORESTS_AND_TREES($offset: Int, $limit: Int){
//   allForests(offset: $offset, first: $limit) {
//     totalCount
//     nodes {
//       forestName
//       id
//       nodeId
//       forestGeoLat
//       forestGeoLong
//       forestGeoRadius
//       forestTreesByForestId {
//         nodes {
//           id
//           nodeId
//           forestTreeName
//           forestTreeGeoLat
//           forestTreeGeoLong
//         }
//         totalCount
//       }
//     }
//   }
// }
// `;

export const GET_ALL_FORESTS_AND_TREES = gql`
query GET_ALL_FORESTS_AND_TREES{
  allForests{
    totalCount
    nodes {
      forestName
      id
      nodeId
      forestGeoLat
      forestGeoLong
      forestGeoRadius
      forestTreesByForestId {
        nodes {
          id
          nodeId
          forestTreeName
          forestTreeGeoLat
          forestTreeGeoLong
        }
        totalCount
      }
    }
  }
}
`;

export const GET_FOREST_DETAILS_BY_ID = gql`
query GET_FOREST_DETAILS_BY_ID($nodeId:ID!){
  forest(
    nodeId: $nodeId
  ) {
    forestName
    id
    nodeId
    forestDesc
    forestUrl
    forestCarbonoffset
    forestOxygen
    forestGeoLat
    forestGeoLong
    forestTreesByForestId {
      totalCount
      nodes {
        masterPlantspecyByMasterPlantSpeciesId {
          speciesName
        }
      }
    }
    forestSponsorsByForestId {
      nodes {
        sponsorBySponsorId {
          sponsorLogo
          sponsorName
        }
      }
    }
  }
}

`;