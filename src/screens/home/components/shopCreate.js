import React from 'react';
import Stack from '@mui/material/Stack'
import Button from '@mui/material/Button'
import { CustomTypography } from '.';

export const ShopCreate = () => {
    return (
        <Stack
            direction={"row"}
            alignItems={"center"}
            gap={"10px"}
        >
            <CustomTypography color={"#fff"} sx={{ flex: 1 }}>
                If you are a shop owner, add your shop.
            </CustomTypography>
            <Button
                variant={"contained"}
                color={"primary"}
                sx={{
                    borderRadius:"12px",
                    minWidth:"110px",
                    p:"12px"
                }}
            >
                ADD
            </Button>
        </Stack>
    )
}