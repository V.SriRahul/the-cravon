import React from 'react';
import Stack from '@mui/material/Stack'
import Button from '@mui/material/Button'
import styled from '@mui/material/styles/styled';

const CustomButton = styled(Button, {
    shouldForwardProp: prop => prop !== "textTransform"
})(({ theme, textTransform }) => ({
    borderRadius: 12,
    flex: 1,
    paddingInline: theme.spacing(1),
    paddingBlock: 12,
    textTransform
}))

export const FlexButtons = (props) => {

    const {
        outlinedButton = "",
        outlinedButtonAction = null,
        containedButton = "",
        containedButtonAction = null,
        textTransform = "uppercase"
    } = props;

    return (
        <Stack
            direction={"row"}
            alignItems={"center"}
            gap={"10px"}
        >
            {
                outlinedButton &&
                <CustomButton
                    variant={"outlined"}
                    color={"primary"}
                    onClick={outlinedButtonAction}
                    textTransform={textTransform}
                >
                    {outlinedButton}
                </CustomButton>
            }
            {
                containedButton &&
                <CustomButton
                    variant={"contained"}
                    color={"primary"}
                    onClick={containedButtonAction}
                    textTransform={textTransform}
                >
                    {containedButton}
                </CustomButton>
            }
        </Stack>
    )
}