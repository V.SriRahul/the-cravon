import React from "react";
import { Home } from './home';
// import { withNavBars } from "./../../HOCs";

class HomeParent extends React.Component {
  render() {
    return <Home {...this.props} />;
  }
}

export default HomeParent;
