import React from 'react';
import styled from '@mui/material/styles/styled';
import { ShopSearchMap, GetLocation, BottomCard } from '../../components';
import { FlexButtons, ShopCreate } from './components';

const Div = styled('div')({
    width: "100%",
    height: "100%"
})

export const Home = props => {
    return (
        <Div>
            <Div sx={{ position: "relative" }}>
                <ShopSearchMap />
                <BottomCard
                    // backgroundColor={"#3A2F38"}
                    backgroundColor={"#fff"}
                    // height={94}
                    borderRadius={"30px 30px 0 0"}
                >
                    {/* <ShopCreate /> */}
                    <FlexButtons 
                        outlinedButton={"Cancel"}
                        containedButton={"Add"}
                    />
                </BottomCard>
                {/* <GetLocation /> */}
            </Div>
        </Div >
    )
}