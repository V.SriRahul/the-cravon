import React from "react";

export const CoupenWithBadge = (props) => {

    const {
        width = 24,
        height = 20,
        badge = false,
        badgeColor = '#f67280',
        color = '#3a2f38',
    } = props;

    return (
        <svg
            xmlns="http://www.w3.org/2000/svg"
            width={width}
            height={height}
            viewBox="0 0 24 20"
            {...props}
        >
            <g id="Group_4981" data-name="Group 4981" transform="translate(-249 -63)">
                <path id="icons8-voucher" d="M4.75,10A1.762,1.762,0,0,0,3,11.75v2.5A.75.75,0,0,0,3.818,15c-.008,0,.053,0,.182,0a2,2,0,0,1,0,4H3.75a.75.75,0,0,0-.75.75v2.5A1.762,1.762,0,0,0,4.75,24h17.5A1.762,1.762,0,0,0,24,22.25v-2.5a.75.75,0,0,0-.75-.75H23a2,2,0,0,1,0-4h.25a.75.75,0,0,0,.75-.75v-2.5A1.762,1.762,0,0,0,22.25,10Zm0,1.5h17.5a.239.239,0,0,1,.25.25V13.6a3.425,3.425,0,0,0,0,6.8V22.25a.239.239,0,0,1-.25.25H4.75a.239.239,0,0,1-.25-.25V20.4a3.425,3.425,0,0,0,0-6.8V11.75A.239.239,0,0,1,4.75,11.5Zm11.485,1.993a.75.75,0,0,0-.516.227l-5.5,5.5a.75.75,0,1,0,1.061,1.06l5.5-5.5a.75.75,0,0,0-.545-1.287ZM11,13.5a1,1,0,1,0,1,1A1,1,0,0,0,11,13.5Zm5,5a1,1,0,1,0,1,1A1,1,0,0,0,16,18.5Z" transform="translate(246 59)"
                    fill={color}
                />
                {badge && <circle id="Ellipse_4" data-name="Ellipse 4" cx="4" cy="4" r="4" transform="translate(264 64)" fill={badgeColor} stroke="#fff" stroke-width="2" />}
            </g>
        </svg>
    );
};