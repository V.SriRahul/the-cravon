import React from "react";

export const NotificationWithBadge = (props) => {

    const {
        width = 19,
        height = 19,
        badge = false,
        badgeColor = '#f67280',
        color = '#3a2f38',
    } = props;

    return (
        <svg
            xmlns="http://www.w3.org/2000/svg"
            width={width}
            height={height}
            viewBox="0 0 19 23"
            {...props}
        >
            <g id="Group_4654" data-name="Group 4654" transform="translate(-314 -63)">
                <path id="icons8-notification" d="M14.868,4.009a7.824,7.824,0,0,0-7.322,7.879v3.727L6.187,18.35l-.007.015A1.818,1.818,0,0,0,7.8,20.924h4.358a3.077,3.077,0,0,0,6.154,0h4.357A1.818,1.818,0,0,0,24.3,18.365l-.007-.015-1.359-2.734V11.693A7.706,7.706,0,0,0,14.868,4.009Zm.072,1.536a6.141,6.141,0,0,1,6.452,6.147v4.1a.769.769,0,0,0,.08.343L22.9,19.021a.238.238,0,0,1-.232.365H7.8a.238.238,0,0,1-.231-.365h0L9,16.138a.769.769,0,0,0,.08-.343V11.888A6.279,6.279,0,0,1,14.94,5.546ZM13.7,20.924h3.077a1.538,1.538,0,0,1-3.077,0Z"
                    transform="translate(307.985 61.999)"
                    fill={color}
                />
                {badge && <circle id="Ellipse_1" data-name="Ellipse 1" cx="4" cy="4" r="4" transform="translate(324 64)" fill={badgeColor} stroke="#fff" stroke-width="2" />}
            </g>
        </svg>
    );
};
