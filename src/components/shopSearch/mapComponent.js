import React, { useEffect } from 'react';
import { CustomControls } from './customControls';
// import Typography from '@mui/material/Typography';
// import styled from '@mui/material/styles/styled';
import './index.css';

// const Title = styled(Typography)({
//     fontWeight: 500,
//     fontSize: 18,
//     color: '#324052',
//     marginLeft: '16px'
// });

export const MapComponent = ({
    map,
    center,
    setMap,
    onIdle,
    onClick,
    // zoom_changed,
    children,
    duration,
    ...props
}) => {

    const ref = React.useRef(null);

    // const [map, setMap] = React.useState();

    useEffect(() => {
        if (ref?.current && !map) {
            setMap(new window.google.maps.Map(ref.current, {}));
        }
        if (map) {
            map.setOptions(props);
            if (map && (center?.lat && center?.lng) && (map?.getCenter()?.lat() !== center?.lat && map?.getCenter()?.lng() !== center?.lng)) {
                map.setCenter(center)
            }
            // map.panTo(props?.center);
           
            map.controls[window.google.maps.ControlPosition.RIGHT_TOP].push(
                document.querySelector(".rightSideIcons")
            );


            //find whether document is full screen or not
            let mapWrapper = document?.getElementsByClassName('map-wrapper')?.[0];
            document.onwebkitfullscreenchange =
                document.onmsfullscreenchange =
                document.onmozfullscreenchange =
                document.onfullscreenchange =
                function () {
                    if (isFullscreen(mapWrapper)) {
                        mapWrapper.classList.add("is-fullscreen");
                    } else {
                        mapWrapper.classList.remove("is-fullscreen");
                    }
                };
        }
        // eslint-disable-next-line
    }, [ref, map, center]);

    //useEffect fir event handlers
    useEffect(() => {
        if (map) {
            ["idle", "click"].forEach((eventName) =>
                window.google.maps.event.clearListeners(map, eventName)
            );

            if (onIdle) {
                map.addListener("idle", () => onIdle(map));
            }

            if (onClick) {
                map.addListener("click", (e) => onClick(e));
            }

        }
    }, [map, onIdle, onClick]);

    const zoomIn = () => {
        map.setZoom(map.getZoom() + 1);
    }

    const zoomOut = () => {
        map.setZoom(map.getZoom() - 1);
    }

    const fullScreenControl = () => {
        const elementToSendFullscreen = document?.getElementsByClassName('map-wrapper')?.[0];
        if (!isFullscreen(elementToSendFullscreen)) {
            requestFullscreen(elementToSendFullscreen);
        }
    }

    // const ExitFullScreen = () => {
    //     exitFullscreen();
    // }

    const isFullscreen = (element) => {
        return (
            (document.fullscreenElement ||
                document.webkitFullscreenElement ||
                document.mozFullScreenElement ||
                document.msFullscreenElement) === element
        );
    }

    const requestFullscreen = (element) => {
        if (element.requestFullscreen) {
            element.requestFullscreen();
        } else if (element.webkitRequestFullScreen) {
            element.webkitRequestFullScreen();
        } else if (element.mozRequestFullScreen) {
            element.mozRequestFullScreen();
        } else if (element.msRequestFullScreen) {
            element.msRequestFullScreen();
        }
    }

    // const exitFullscreen = () => {
    //     if (document.exitFullscreen) {
    //         document.exitFullscreen();
    //     } else if (document.webkitExitFullscreen) {
    //         document.webkitExitFullscreen();
    //     } else if (document.mozCancelFullScreen) {
    //         document.mozCancelFullScreen();
    //     } else if (document.msExitFullscreen) {
    //         document.msExitFullscreen();
    //     }
    // }

    const getCurrentLocation = () => {
        // Try HTML5 geolocation.

        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(
                (position) => {
                    const pos = {
                        lat: position.coords.latitude,
                        lng: position.coords.longitude,
                    };
                   
                    map.setCenter(pos);
                },
                () => {
                    handleLocationError(true, map.getCenter());
                }
            );
        } else {
            // Browser doesn't support Geolocation
            handleLocationError(false, map.getCenter());
        }
    }

    const handleLocationError = (browserHasGeolocation, pos) => {
       console.log(browserHasGeolocation, pos)
    }

    const changeMapTypes = (value) => {
        map.setMapTypeId(value);
    }

    return (
        <div className='map-wrapper'>
            <div ref={ref} id="map" style={{ flexGrow: "1", height: "100%" }}>
                {React.Children.map(children, (child) => {
                    if (React.isValidElement(child)) {
                        // set the map prop on the child component
                        return React.cloneElement(child, { map });
                    }
                })}
            </div>
            {/* Hide controls until they are moved into the map */}
            <div style={{ display: 'none' }}>
                <CustomControls
                    zoomIn={zoomIn}
                    zoomOut={zoomOut}
                    fullScreenControl={fullScreenControl}
                    getCurrentLocation={getCurrentLocation}
                    changeMapTypes={changeMapTypes}
                />
            </div>
        </div>
    )
}