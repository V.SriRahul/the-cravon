import React, { useCallback, useContext, useEffect, useState } from 'react';
import { Wrapper, Status, } from "@googlemaps/react-wrapper";
import { MapComponent } from './mapComponent';
import { Marker } from './marker';
import { Circle } from './circle';
import TreeHuggerOrange from '../../assets/tree_hugger_orange.svg'
import TreeHuggerGreen from '../../assets/tree_hugger_green.svg'
import { InfoDetails } from './infoDetails';
import mapStyles from './mapStyles.json';
import Skeleton from '@mui/material/Skeleton';
import styled from '@mui/material/styles/styled';
import { DialogContext } from '../../contexts';
import { HoverDetails } from './hoverDetails';
// import { config } from '../../config';
import { useApolloClient } from "@apollo/client";
import { GET_TREES_BY_NODEID, GET_ALL_FORESTS_AND_TREES, GET_FOREST_DETAILS_BY_ID } from '../../graphql/queries';
// import { loadScript } from '../../utils';
// import { Overlay } from './overlay';
import Stas from "./stasHeader";
import Logo from '../../assets/logo.png';

const CustomSkeleton = styled(Skeleton)({
    minHeight: '100%',
    minWidth: '100%',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    transform: 'unset',
    backgroundColor: "#fff",
    '& img': {
        visibility: "visible",
        maxWidth: "100%",
        height: "auto",
        padding: 10
    }
})

let initialZoom = 9, minZoom = 3;

let dialogStyle = [{
    '& .MuiDialogContent-root': {
        padding: 0
    }
}]

export const ShopSearchMap = (props) => {


    const dialog = useContext(DialogContext);

    const [center, setCenter] = useState({
        "lat": 13.049799526425561,
        "lng": 80.09315294503136
    });


    // const [loading, setLoading] = useState(null);

    const [map, setMap] = React.useState();

    const [selectedNode, setSelectedNode] = useState({
        marker: null,
        // circle: null
    })

    const onIdle = (map) => {
        // console.log('idle');
    };

    const onClickMap = (e) => {
        console.log('Click Map')
    }

    const dialogComponent = (component) => {
        dialog.setDialog({
            ...dialog,
            open: true,
            body: component,
            container: document.querySelector('#map'),
            sx: dialogStyle
        })
    }

    const markerClick = async (nodeId, circleNodeId) => {

        setSelectedNode({
            marker: nodeId,
            // circle: circleNodeId
        })

        dialogComponent(<InfoDetails loading showVideo />)
    }

    const closeDialog = () => {
        dialog.setDialog({
            open: false
        })
    }

    const renderStatus = (status) => {
        if (status === Status.FAILURE) return <CustomSkeleton component="div">Something went wrong!</CustomSkeleton>;
        return <CustomSkeleton component="div" variant="rectangular">
            <img src={Logo} alt={"logo"} />
        </CustomSkeleton>;
    };


    const markerIcon = useCallback((url) => ({
        url,
        // anchor: new window.google.maps.Point(0,0),
        // origin: new window.google.maps.Point(0,0),
        // size: new window.google.maps.Size(50, 50)

        // eslint-disable-next-line
    }), [selectedNode.marker])

    return (
        <div style={{ height: '100%', width: '100%' }}>
            {/* api key initialize in index.html in script tag */}
            <Wrapper render={renderStatus}>
                <MapComponent
                    center={center}
                    zoom={initialZoom}
                    minZoom={minZoom}
                    // maxZoom={maxZoom}
                    onIdle={onIdle}
                    onClick={onClickMap}
                    // zoom_changed={zoom_changed}
                    disableDefaultUI={true}
                    keyboardShortcuts={false}
                    styles={[...mapStyles]}
                    map={map}
                    setMap={setMap}
                >
                    <Marker
                        // key={`marker-${treeIndex}`}
                        position={{
                            "lat": 13.049799526425561,
                            "lng": 80.09315294503136
                        }}
                        // icon={selectedNode.marker === tree?.nodeId ? markerIcon(TreeHuggerOrange) : markerIcon(TreeHuggerGreen)}
                        // icon={markerIcon(TreeHuggerOrange)}
                        // markerClick={() => markerClick(tree?.nodeId, _?.nodeId)}
                        map={map}
                    />
                </MapComponent>
            </Wrapper>
        </div>
    )
}