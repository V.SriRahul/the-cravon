import React from 'react';
import styled from '@mui/material/styles/styled';
import Typography from '@mui/material/Typography';
import Stack from '@mui/material/Stack';
import Skeleton from '@mui/material/Skeleton';
import CloseIcon from '@mui/icons-material/Close';
import Divider from '@mui/material/Divider';
import Button from '@mui/material/Button';
import { Carousel } from '../carousel';
import { Grid } from '@mui/material';
import { ShareIcon } from '../svg';

const CustomTypography = styled(Typography, {
    shouldForwardProp: (prop) => prop !== "color" && prop !== 'fontSize' && prop !== 'fontWeight' && prop !== 'marginBottom' && prop !== 'fontFamily'
})(({ color = "#363A57", fontSize = 12, fontWeight, marginBottom, fontFamily }) => ({
    color,
    fontSize,
    fontWeight,
    marginBottom,
    fontFamily
}));

const CustomBox = styled('div', {
    shouldForwardProp: prop => prop !== "padding" && prop !== "backgroundColor" && prop !== "width" && prop !== "height" && prop !== "borderRadius" && prop !== "marginBottom"
})(({ padding, backgroundColor, width = "128px", height = "70px", borderRadius = 6, marginBottom }) => ({
    padding,
    backgroundColor,
    width,
    height,
    borderRadius,
    marginBottom,
    overflow: "hidden"
}))

const CustomImg = styled('img')({
    maxWidth: '100%',
    width: '100%',
    height: '100%',
    objectFit: 'cover',
})

const SponsoredLogo = styled('div')({
    width: 24,
    height: 24,
    overflow: "hidden"
})

const AvatarWrapper = styled('div')({
    height: 120,
    overflow: "hidden",
    borderRadius: "8px",
    marginBlock: "8px",
    position: "relative"
})

const AvatarName = styled('div')({
    borderRadius: "6px",
    padding: "6px 12px",
    color: "#FFFFFF",
    fontFamily: "crayond_bold",
    fontSize: 12,
    backgroundColor: "#0000005e",
    position: "absolute",
    bottom: 8,
    left: 8,
})

const Box = styled('div', {
    shouldForwardProp: prop => prop !== "width"
})(({ theme, width = "100%" }) => ({
    width,
    "@media (max-width:375px)": {
        width: '100%',
    }
}))

const CustomDivider = styled(Divider)({
    marginBlock: '12px'
})

export const InfoDetails = (props) => {

    const {
        title = "",
        description = "",
        logo = "",
        showVideo = false,
        showImage = false,
        videos = [],
        subTitle = "",
        info = [],
        width,
        onClose = null,
        status = "",
        loading = false,
        sponsoredName = "",
        sponsoredLogo = "",
        avatarUrl = "",
        avatarName = "",
        share = false
    } = props;

    return (
        <Box width={width}>
            {
                loading ? (
                    <div style={{ padding: 16 }}>
                        <Stack
                            direction="row"
                            alignItems={"center"}
                            sx={{ mb: 1 }}
                        >
                            <Skeleton variant="text" width={"30%"}></Skeleton>
                            <div style={{ flex: 1 }} />
                            <Skeleton variant="circular" width={20} height={20}></Skeleton>
                        </Stack>
                        {/* <Skeleton variant="rectangular" width={30} height={10}></Skeleton> */}
                        <Skeleton variant="text" width={"100%"}></Skeleton>
                        <Skeleton variant="text" width={"100%"}></Skeleton>
                        {showVideo && <Skeleton sx={{ my: 2, borderRadius: '6px' }} variant="rectangular" width={'100%'} height={164}></Skeleton>}
                        <Skeleton sx={{ mb: 1 }} variant="text" width={"30%"}></Skeleton>
                        <Stack
                            direction="row"
                            alignItems={"center"}
                            gap={"10px"}
                        >
                            {[1, 2].map((_, i) => (
                                <Skeleton key={i} sx={{ borderRadius: '6px' }} variant="rectangular" width={132} height={70}></Skeleton>
                            ))}
                        </Stack>
                    </div>
                ) : (
                    <>
                        <Stack
                            direction="row"
                            alignItems={"center"}
                            justifyContent={"space-between"}
                            padding={2}
                            paddingBottom={1}
                            position={"sticky"}
                            top={0}
                            sx={{
                                backgroundColor: "#fff",
                                zIndex: 1
                            }}
                        >
                            <Stack
                                direction="row"
                                alignItems={"center"}
                                gap={"10px"}
                            >
                                {title && <CustomTypography fontFamily={"crayond_bold"} fontSize={16} color="#222622" >{title}</CustomTypography>}
                                {logo && <CustomImg>{logo}</CustomImg>}
                            </Stack>
                            <div style={{ cursor: "pointer" }} onClick={onClose} >
                                <CloseIcon fontSize='small' />
                            </div>
                        </Stack>
                        <div style={{ padding: 16, paddingTop: 0, lineHeight: 1 }}>
                            {status &&
                                <CustomTypography fontFamily={"crayond_regular"} color="#262626" variant='caption' component={"span"}>
                                    <CustomTypography component={"span"} fontFamily={"crayond_medium"} color="#262626">{'Nickname : '}</CustomTypography>
                                    {status}
                                </CustomTypography>
                            }
                            {description &&
                                <CustomTypography marginBottom={"8px"} color="#505750" fontFamily={"crayond_regular"}>{description}</CustomTypography>
                            }

                            {(sponsoredName || sponsoredLogo) &&
                                <>
                                    <CustomTypography marginBottom={"8px"} color="#222622" fontFamily={"crayond_bold"} fontSize={14}>{"Sponsored by"}</CustomTypography>
                                    <Stack
                                        direction="row"
                                        alignItems={"center"}
                                        gap={"12px"}
                                        sx={{
                                            border: "1px solid #F1F1F1",
                                            borderRadius: "6px",
                                            padding: "12px"
                                        }}
                                    >
                                        {sponsoredLogo &&
                                            <SponsoredLogo>
                                                <CustomImg src={sponsoredLogo} alt={"sponsorLogo"} />
                                            </SponsoredLogo>
                                        }
                                        <CustomTypography color="#222622" fontFamily={"crayond_medium"} fontSize={12} >{sponsoredName ? sponsoredName : '-'}</CustomTypography>
                                    </Stack>
                                </>
                            }
                            <CustomDivider />
                            {(showVideo) &&
                                <>
                                    <Stack
                                        direction="row"
                                        alignItems={"center"}
                                        justifyContent={"space-between"}
                                        marginBottom={"8px"}
                                    >
                                        <CustomTypography color="#222622" fontFamily={"crayond_bold"} fontSize={14} >{'Videos'}</CustomTypography>
                                        {/* <CustomTypography>{'See all'}</CustomTypography> */}
                                    </Stack>
                                    {
                                        <Carousel videos={videos} height={164} />
                                    }
                                    <CustomDivider />
                                </>
                            }
                            {
                                (showImage && avatarUrl) &&
                                <>
                                    <CustomTypography color="#222622" fontFamily={"crayond_bold"} fontSize={14} >{'Planted by'}</CustomTypography>
                                    <AvatarWrapper>
                                        <CustomImg
                                            src={avatarUrl}
                                            alt={"plantedBy"}
                                            sx={{
                                                filter: 'brightness(0.6)'
                                            }}
                                        />
                                        {avatarName && <AvatarName>{avatarName}</AvatarName>}
                                    </AvatarWrapper>
                                </>
                            }
                            {subTitle && <CustomTypography marginBottom={"8px"} color="#222622" fontFamily={"crayond_bold"} fontSize={14} >{subTitle}</CustomTypography>}
                            {
                                <div style={{ flex: 1 }}>
                                    <Grid container spacing={1}>
                                        {
                                            info?.length && info?.map((_, i) => (
                                                <Grid item xs={6} key={i}>
                                                    <CustomBox
                                                        key={i}
                                                        backgroundColor="#F1F1F1"
                                                        padding="12px"
                                                        width="100%"
                                                        sx={{
                                                            display: 'flex',
                                                            flexDirection: 'column',
                                                            justifyContent: "center",
                                                        }}
                                                    >
                                                        <CustomTypography sx={{ flex: 1 }} color="#222622" fontFamily={"crayond_bold"} fontSize={14} >{_?.label ?? '-'}</CustomTypography>
                                                        <CustomTypography color="#222622" fontFamily={"crayond_medium"} >
                                                            {_?.value ? _?.value : '-'}
                                                            {_?.unit ? ` ${_?.unit}` : ''}
                                                        </CustomTypography>
                                                    </CustomBox>
                                                </Grid>

                                            ))
                                        }
                                    </Grid>
                                </div>
                            }
                        </div>
                        {
                            share &&
                            <Button
                                variant="contained"
                                fullWidth
                                // onClick={buttonAction}
                                sx={{
                                    backgroundColor: "#CBF6C8",
                                    borderRadius:0,
                                    color: "#17970E",
                                    fontFamily: "crayond_medium",
                                    textTransform: "capitalize",
                                    p: "10px",
                                    boxShadow: "none",
                                    position:"sticky",
                                    bottom:0,
                                    '&:hover': {
                                        backgroundColor: "#CBF6C8",
                                        color: "#17970E",
                                        boxShadow: "none",
                                    }
                                }}
                            >
                                <ShareIcon style={{ marginRight: 8 }} />
                                {'Share'}
                            </Button>
                        }
                    </>
                )
            }

        </Box >
    )
}