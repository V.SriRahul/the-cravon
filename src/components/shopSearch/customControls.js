import React, { useState } from 'react';
import { NotificationWithBadge, CoupenWithBadge } from '../../components/svg';
import default_map from '../../assets/default_map.png'
import satellite_map from '../../assets/satellite_map.png'
import hybrid_map from '../../assets/hybrid_map.png'
import IconButton from '@mui/material/IconButton';
import './index.css';

const MAPTYPEOPTIONS = [
    { label: 'Default', value: 'roadmap', url: default_map },
    { label: 'Satellite', value: 'satellite', url: satellite_map },
    { label: 'Hybrid', value: 'hybrid', url: hybrid_map }
]

export const CustomControls = (props) => {

    const [state, setState] = useState({
        mapType: MAPTYPEOPTIONS[0],
        toggle: false
    })

    const onChangeState = (key, value) => {
        setState({
            ...state, [key]: value
        })
    }

    const onChangeMapType = (val) => {
        setState({
            ...state,
            mapType: val,
            toggle: false
        })
        props?.changeMapTypes && props?.changeMapTypes(val.value)
    }

    return (
        <div>
            <div className="products_list"></div>
            <div className="rightSideIcons">
                <IconButton className='icons mr-10'>
                    <CoupenWithBadge />
                </IconButton>
                <IconButton className='icons'>
                    <NotificationWithBadge />
                </IconButton>
            </div>
            <div className="map-types">
                <div className='mapTypeflex'>
                    <div className='mapTypeCard' style={{ border: '1px solid #22DACE' }} onClick={() => onChangeState('toggle', !state?.toggle)}>
                        <img src={state?.mapType.url} alt={state?.mapType.url + "img"} />
                        <div className='absoluteDiv'>{state?.mapType.label}</div>
                    </div>
                    {state?.toggle && MAPTYPEOPTIONS?.filter(({ label }) => label !== state?.mapType?.label)?.map(_ => (
                        <div className='mapTypeCard' key={_.label} onClick={() => onChangeMapType(_)}>
                            <img src={_.url} alt={_.url + "img"} />
                            <div className='absoluteDiv'>{_.label}</div>
                        </div>
                    ))}
                </div>
            </div>
        </div>
    )
}