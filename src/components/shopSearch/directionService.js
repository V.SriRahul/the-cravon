import { useState, useEffect } from 'react';

export const DirectionServices = (props) => {

    const [directionsService, setDirectionsService] = useState();
    const [directionsRenderer, setDirectionsRenderer] = useState();

    useEffect(() => {
        if (!directionsService) {
            setDirectionsService(new window.google.maps.DirectionsService())
        }
        if (!directionsRenderer) {
            setDirectionsRenderer(new window.google.maps.DirectionsRenderer())
        }
        return () => {
            if (directionsRenderer) {
                directionsRenderer.setMap(null)
            }
        }

    }, [directionsService, directionsRenderer])

    useEffect(() => {
        if (directionsRenderer) {
            //checking directionsRenderer to set map
            if (!directionsRenderer?.getMap()) {
                directionsRenderer.setMap(props?.map);
            }
            //checking props to set in directionsRenderer 
            if (
                !(directionsRenderer?.polylineOptions?.strokeColor === "#363A57" &&
                    directionsRenderer?.suppressMarkers)
            ) {
                directionsRenderer.setOptions({
                    polylineOptions: {
                        strokeColor: '#363A57'
                    },
                    suppressMarkers: true,
                })
            }
            //checking directionsRenderer whether have same origin and destionation
            let findDirections = directionsRenderer?.getDirections();
            if (findDirections?.request) {
                const { request } = findDirections;
                if (
                    request?.origin?.location?.lat() === props?.origin?.lat &&
                    request?.origin?.location?.lng() === props?.origin?.lng &&
                    request?.destination?.location?.lat() === props?.destination?.lat &&
                    request?.destination?.location?.lng() === props?.destination?.lng

                ) {
                    return
                }
            }
            calculateAndDisplayRoute(props)
        }

    }, [props])

    const calculateAndDisplayRoute = (options) => {
        const { origin, destination } = options;
        if (origin && destination) {
            directionsService
                .route({
                    origin: new window.google.maps.LatLng(origin.lat, origin.lng),
                    destination: new window.google.maps.LatLng(destination.lat, destination.lng),
                    travelMode: window.google.maps.TravelMode.DRIVING,
                })
                .then((response) => {
                    directionsRenderer.setDirections(response);
                    debugger
                    options?.sendDirectionsData && options.sendDirectionsData(response?.routes?.[0]?.legs?.[0])
                    options?.moveOrigin(response?.routes?.[0]?.legs?.[0]?.steps)
                })
                .catch((e) => console.log("Directions request failed due to " + e));
        }
    }

    return null
}