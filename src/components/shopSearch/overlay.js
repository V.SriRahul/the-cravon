import { useState, useEffect } from 'react';
import ReactDOMServer from "react-dom/server";

export const Overlay = (props) => {

    const {
        bounds,
        radius,
        bearing
    } = props;

    let div;

    const [overlay, setOverlay] = useState(null);

    useEffect(() => {
        if (!overlay) {
            setOverlay(new window.google.maps.OverlayView({}));
        }
        // remove Overlay from map on unmount
        return () => {
            if (overlay) {
                overlay.setMap(null);
            }
        };
        // eslint-disable-next-line
    }, [overlay]);


    useEffect(() => {
        if (overlay) {
            // overlay.onAdd(creation);
            overlay.onAdd = () => {
                creation()
            };

            overlay.draw = () => {
                draw()
            };
            if (!overlay.getMap()) {
                overlay.setMap(props?.map);
            }

            // overlay.setOptions(props);
        }
        // eslint-disable-next-line
    }, [overlay, props]);

    const creation = () => {
        div = document.createElement("div");
        div.innerHTML=ReactDOMServer.renderToString(props?.component);
        div.style.position = "absolute";
        const panes = overlay.getPanes();
        panes.overlayLayer.appendChild(div);
    }

    const draw = () => {
        const overlayProjection = overlay.getProjection();
        const findPosition = overlayProjection.fromLatLngToDivPixel(window.google.maps.geometry.spherical.computeOffset(bounds, radius, bearing));
        if (div) {
            div.style.left = findPosition.x + "px";
            div.style.top = findPosition.y + "px";
        }
    }

    return null;
}