import React, { useCallback, useState } from 'react';
import { Wrapper, Status, } from "@googlemaps/react-wrapper";
import { MapComponent } from './mapComponent';
import { Marker } from './marker';
import mapStyles from '../shopSearch/mapStyles.json';
import Skeleton from '@mui/material/Skeleton';
import styled from '@mui/material/styles/styled';
import Logo from '../../assets/logo.png';
import GPSLocationIcon from '../../assets/location.png';

const CustomSkeleton = styled(Skeleton)({
    minHeight: '100%',
    minWidth: '100%',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    transform: 'unset',
    backgroundColor: "#fff",
    '& img': {
        visibility: "visible",
        maxWidth: "100%",
        height: "auto",
        padding: 10
    }
})

let initialZoom = 9, minZoom = 3;

let initialLocation = {
    "lat": 13.049799526425561,
    "lng": 80.09315294503136
}

export const GetLocation = (props) => {

    const [center, setCenter] = useState(initialLocation);

    const [map, setMap] = useState();
    const [location, setLocation] = useState(initialLocation);

    const onIdle = (map) => {
        // console.log('idle');
    };

    const onClickMap = (e) => {
        onChangeState('location', e.latLng)
    }

    const renderStatus = (status) => {
        if (status === Status.FAILURE) return <CustomSkeleton component="div">Something went wrong!</CustomSkeleton>;
        return <CustomSkeleton component="div" variant="rectangular">
            <img src={Logo} alt={"logo"} />
        </CustomSkeleton>;
    };


    const markerIcon = {
        url:GPSLocationIcon,
        // anchor: new window.google.maps.Point(0,0),
        // origin: new window.google.maps.Point(0,0),
        // size: new window.google.maps.Size(50, 50)

        // eslint-disable-next-line
    }

    const onChangeState = (key, value) => {
        if (key === "location") {
            setLocation(value);
            markerDragEnd(value)
            map.panTo(value);
            if (map.getZoom() < 8) {
                map.setZoom(8)
            }
        }
    }

    const markerDragEnd = (position) => {
        let geocoder = new window.google.maps.Geocoder();
        geocoder.geocode({ latLng: position }, function (results, status) {
            debugger
            if (status == window.google.maps.GeocoderStatus.OK) {
                console.log(results, status)
            }
        }
        );
    }

    return (
        <div style={{ height: '100%', width: '100%' }}>
            {/* api key initialize in index.html in script tag */}
            <Wrapper render={renderStatus}> 
                <MapComponent
                    center={center}
                    // defaultCenter={initialLocation}
                    zoom={initialZoom}
                    minZoom={minZoom}
                    // maxZoom={maxZoom}
                    onIdle={onIdle}
                    onClick={onClickMap}
                    // zoom_changed={zoom_changed}
                    disableDefaultUI={true}
                    keyboardShortcuts={false}
                    styles={[...mapStyles]}
                    map={map}
                    setMap={setMap}
                    onChangeState={(latLng) => onChangeState('location', latLng)}
                >
                    <Marker
                        position={location}
                        draggable
                        icon={markerIcon}
                        map={map}
                        markerDragEnd={markerDragEnd}
                    />
                </MapComponent>
            </Wrapper>
        </div>
    )
}