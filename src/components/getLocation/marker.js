import { useState, useEffect } from 'react';

export const Marker = (props) => {

    const [marker, setMarker] = useState();

    useEffect(() => {
        if (!marker) {
            setMarker(new window.google.maps.Marker({
                animation: window.google.maps.Animation.DROP
            }));
        }
        // remove marker from map on unmount
        return () => {
            if (marker) {
                marker.setMap(null);
            }
        };
        // eslint-disable-next-line
    }, [marker]);

    useEffect(() => {
        if (marker) {
            if (!marker.getMap()) {
                marker.setMap(props?.map);
            }
            marker.setOptions(props);
        }
    }, [marker, props])

    useEffect(() => {

        if (marker) {
            marker.addListener("dragend", () => {
                if(props?.markerDragEnd){
                    props.markerDragEnd(marker.getPosition())
                }
            });
        }
        // eslint-disable-next-line
    }, [marker])

    return null;
}