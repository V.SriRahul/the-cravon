import React, { useEffect } from 'react';
import { CustomControls } from './customControl';
import './index.css';

export const MapComponent = ({
    map,
    center,
    setMap,
    onIdle,
    onClick,
    // zoom_changed,
    children,
    duration,
    ...props
}) => {

    const ref = React.useRef(null);
    let autocomplete;

    // const [map, setMap] = React.useState();

    useEffect(() => {
        if (ref?.current && !map) {
            setMap(new window.google.maps.Map(ref.current, {}));
        }
        if (map) {
            map.setOptions(props);
            if (map && (center?.lat && center?.lng) && (map?.getCenter()?.lat() !== center?.lat && map?.getCenter()?.lng() !== center?.lng)) {
                map.setCenter(center)
            }
            // map.panTo(props?.center);
            map.controls[window.google.maps.ControlPosition.RIGHT_BOTTOM].push(
                document.querySelector(".controls")
            );
            map.controls[window.google.maps.ControlPosition.RIGHT_TOP].push(
                document.querySelector(".fullscreen-control")
            );
            map.controls[window.google.maps.ControlPosition.TOP_CENTER].push(
                document.querySelector(".autoComplete")
            );

            //find whether document is full screen or not
            let mapWrapper = document?.getElementsByClassName('map-wrapper')?.[0];
            document.onwebkitfullscreenchange =
                document.onmsfullscreenchange =
                document.onmozfullscreenchange =
                document.onfullscreenchange =
                function () {
                    if (isFullscreen(mapWrapper)) {
                        mapWrapper.classList.add("is-fullscreen");
                    } else {
                        mapWrapper.classList.remove("is-fullscreen");
                    }
                };
        }
        // eslint-disable-next-line
    }, [ref, map, center]);

    useEffect(() => {
        if (map) {
            const input = document.querySelector("#auto_complete");
            const options = {
                fields: ["formatted_address", "geometry", "name", "address_components"],
                strictBounds: false,
                // types: ["address"],
            };
            autocomplete = new window.google.maps.places.Autocomplete(input, options);
            autocomplete.bindTo("bounds", map);

            autocomplete.addListener("place_changed", () => {
                const place = autocomplete.getPlace();
                if (props?.onChangeState) {
                    props.onChangeState({
                        lat: place.geometry.location.lat(),
                        lng: place.geometry.location.lng(),
                    })
                }
            })
        }
    }, [map, autocomplete])

    //useEffect fir event handlers
    useEffect(() => {
        if (map) {
            ["idle", "click"].forEach((eventName) =>
                window.google.maps.event.clearListeners(map, eventName)
            );

            if (onIdle) {
                map.addListener("idle", () => onIdle(map));
            }

            if (onClick) {
                map.addListener("click", (e) => onClick(e));
            }

        }
    }, [map, onIdle, onClick]);

    const fullScreenControl = () => {
        const elementToSendFullscreen = document?.getElementsByClassName('map-wrapper')?.[0];
        if (!isFullscreen(elementToSendFullscreen)) {
            requestFullscreen(elementToSendFullscreen);
        }
    }

    // const ExitFullScreen = () => {
    //     exitFullscreen();
    // }

    const isFullscreen = (element) => {
        return (
            (document.fullscreenElement ||
                document.webkitFullscreenElement ||
                document.mozFullScreenElement ||
                document.msFullscreenElement) === element
        );
    }

    const requestFullscreen = (element) => {
        if (element.requestFullscreen) {
            element.requestFullscreen();
        } else if (element.webkitRequestFullScreen) {
            element.webkitRequestFullScreen();
        } else if (element.mozRequestFullScreen) {
            element.mozRequestFullScreen();
        } else if (element.msRequestFullScreen) {
            element.msRequestFullScreen();
        }
    }

    // const exitFullscreen = () => {
    //     if (document.exitFullscreen) {
    //         document.exitFullscreen();
    //     } else if (document.webkitExitFullscreen) {
    //         document.webkitExitFullscreen();
    //     } else if (document.mozCancelFullScreen) {
    //         document.mozCancelFullScreen();
    //     } else if (document.msExitFullscreen) {
    //         document.msExitFullscreen();
    //     }
    // }

    const getCurrentLocation = () => {

        if ("geolocation" in navigator) {
            const success = (position) => {
                props.onChangeState({
                    lat: position?.coords?.latitude,
                    lng: position?.coords?.longitude,
                })
            }
            const error = (error) => {
                //   console.log(error)
                alert(error?.message)
            }

            navigator.geolocation.getCurrentPosition(success, error);
        }
    }


    return (
        <div className='map-wrapper'>
            <div ref={ref} id="map" style={{ flexGrow: "1", height: "100%" }}>
                {React.Children.map(children, (child) => {
                    if (React.isValidElement(child)) {
                        // set the map prop on the child component
                        return React.cloneElement(child, { map });
                    }
                })}
            </div>
            {/* Hide controls until they are moved into the map */}
            <div style={{ display: 'none' }}>
                <CustomControls
                    fullScreenControl={fullScreenControl}
                    getCurrentLocation={getCurrentLocation}
                />
            </div>
        </div>
    )
}