import React, { useState } from 'react';
import { FullScreenIcon,LocationIcon } from '../../components/svg';
import './index.css';

export const CustomControls = (props) => {

    const [state, setState] = useState({
        placeSearch: ""
    })

    const onChangeState = (key, value) => {
        setState({
            ...state, [key]: value
        })
    }

    return (
        <div>
            <div className="autoComplete">
                <input
                    id="auto_complete"
                    type="text"
                    value={state?.placeSearch}
                    onChange={e => onChangeState('placeSearch', e.target.value)}
                />
            </div>
            <div className="fullscreen-control">
                <div className='icon-wrapper' onClick={props?.fullScreenControl}>
                    <FullScreenIcon />
                </div>
            </div>
            <div className="controls">
                <div className='icon-wrapper' onClick={props?.getCurrentLocation}>
                    <LocationIcon />
                </div>
            </div>
        </div>
    )
}