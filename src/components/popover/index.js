import Popover from '@mui/material/Popover';
import React from 'react';

export const PopoverComponent = (props) => {

    const {
        id = "",
        open = false,
        anchorEl = null,
        handleClose = null,
        anchorOrigin = {
            vertical: 'bottom',
            horizontal: 'left',
        },
        transformOrigin = {
            vertical: 'top',
            horizontal: 'center',
        },
        component = <></>
    } = props;

    debugger

    return (
        <Popover
            id={id}
            open={open}
            anchorEl={anchorEl}
            onClose={handleClose}
            anchorOrigin={anchorOrigin}
            transformOrigin={transformOrigin}
        >
            {component}
        </Popover>
    )
}