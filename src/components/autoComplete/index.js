import { Autocomplete, TextField } from '@mui/material';
import React from 'react';

export const AutoCompleteSelect = (props) => {

    const {
        value = null,
        onChange = null,
        options = [],
        width,
        placeholder="Search..."
    } = props;

    return (
        <Autocomplete
            value={value}
            onChange={(event, newValue) => {
                onChange(newValue);
            }}
            options={options}            
            sx={[{
                width,
                '& .MuiOutlinedInput-root': {
                    p: 0
                }
            }]}
            renderInput={(params) => <TextField {...params} placeholder={placeholder} />}
        />
    )
}