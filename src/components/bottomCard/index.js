import React from 'react';
import styled from '@mui/material/styles/styled';

const FixedDiv = styled('div', {
    shouldForwardProp: prop => prop !== "backgroundColor" && prop !== "boxShadow" && prop !== "height" && prop !== "borderRadius"
})(({ theme, boxShadow, backgroundColor, height, borderRadius }) => ({
    position: 'absolute',
    inset: 'auto 0 0 0',
    boxShadow,
    backgroundColor,
    height,
    borderRadius,
    padding: theme.spacing(3)
}))
export const BottomCard = ({ children = "", boxShadow, backgroundColor, borderRadius, height }) => {
    return (
        <FixedDiv
            height={height}
            boxShadow={boxShadow}
            backgroundColor={backgroundColor}
            borderRadius={borderRadius}
        >
            {children}
        </FixedDiv>
    )
}
